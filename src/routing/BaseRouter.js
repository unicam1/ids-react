import React from "react";
import {BrowserRouter, Route} from "react-router-dom";
import LoginScreen from "../screens/LoggedOut/LoginScreen";
import RegisterScreen from "../screens/LoggedOut/RegisterScreen";
import DashboardSuperAdmin from "../screens/LoggedIn/Admin/Dashboard";
import DashboardAmministratoreSede from "../screens/LoggedIn/AmministratoreSede/Dashboard";
import DashboardSegretaria from "../screens/LoggedIn/SegretariaSede/Dashboard";
import DashboardPaziente from "../screens/LoggedIn/Paziente/Dashboard";
import DashboardDottore from "../screens/LoggedIn/Dottore/Dashboard";
import NewPatientScreen from "../screens/LoggedOut/NewPatientScreen";
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {AUTH_TOKEN_ID} from "../constants/CommonConstants";


class BaseRouter extends React.Component {
    state = {}

    handleClick = (event) => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {

        this.setState({anchorEl: null});
    };
    logout = () => {

        sessionStorage.setItem(AUTH_TOKEN_ID, null);
        window.location = '/';
    };


    render() {
        let {anchorEl} = this.state;
        let open = anchorEl ? true : false;

        return (
            <BrowserRouter>

                <Route exact path={"/"}>
                    <LoginScreen/>
                </Route>
                <Route exact path={"/EA/dashboard"}>
                    <div style={{position: "absolute", top: 0, right: 10}}><IconButton
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        onClick={this.handleClick}
                    >
                        <MoreVertIcon/>
                    </IconButton></div>
                    <DashboardSuperAdmin/>
                </Route>
                <Route exact path={"/AA/dashboard"}>
                    <div style={{position: "absolute", top: 0, right: 10}}><IconButton
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        onClick={this.handleClick}
                    >
                        <MoreVertIcon/>
                    </IconButton></div>
                    <DashboardAmministratoreSede/>
                </Route>
                <Route exact path={"/DN/dashboard"}>
                    <div style={{position: "absolute", top: 0, right: 10}}><IconButton
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        onClick={this.handleClick}
                    >
                        <MoreVertIcon/>
                    </IconButton></div>
                    <DashboardPaziente/>
                </Route>
                <Route exact path={"/SA/dashboard"}>
                    <div style={{position: "absolute", top: 0, right: 10}}><IconButton
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        onClick={this.handleClick}
                    >
                        <MoreVertIcon/>
                    </IconButton></div>
                    <DashboardSegretaria/>
                </Route>
                <Route path={"/DT/dashboard"}>
                    <div style={{position: "absolute", top: 0, right: 10}}><IconButton
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        onClick={this.handleClick}
                    >
                        <MoreVertIcon/>
                    </IconButton></div>
                    <DashboardDottore/>
                </Route>
                <Route path={"/Register"}>
                    <RegisterScreen/>
                </Route>
                <Route path={"/patient"}>
                    <NewPatientScreen/>
                </Route>
                <Menu
                    id="long-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={open}
                    onClose={this.handleClose}
                    PaperProps={{
                        style: {
                            maxHeight: 100,
                            width: '20ch',
                        },
                    }}
                >
                    <MenuItem onClick={this.logout}>
                        Logout
                    </MenuItem>
                </Menu>
            </BrowserRouter>
        )
    }
}

export default BaseRouter

import React from "react";
import {BreakpointProvider} from "react-socks"

import packageJson from "../package.json";
import BaseRouter from "./routing/BaseRouter";


global.appVersion = packageJson.version;


class App extends React.Component {

    render() {
        return (
            <BreakpointProvider>
                <BaseRouter/>
            </BreakpointProvider>);
    }
}

export default App;


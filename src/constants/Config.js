import BaseAxios from "./ApiHelpers/BaseAxios";
import {GET_ANIME_EPISODE_LIST, GET_ANIME_PICTURES_LIST} from "./ApiHelpers/ApiConst";

export const FILTER = [
    {
        field: "type",
        label: "Type"
    }
]


export const DEFAULT_HOME_API = "/v3/top/anime/1/airing";

export const SEARCH_API = (searchString) => {
    return "/v3/search/anime?q=" + searchString + "&limit=30";
}


/**
 *
 * TODO Place the scructure inside the param tag
 * The structure of the returned object must contain:
 * mal_id: id (String)
 * type: String
 * title: String
 * score: Numeric from 0 to 5 (Optional)
 * synopsis: String (Optional)
 * type: String (Optional)
 * episodes: Numeric (Optional)
 * start_date: date (Optional, momentjs library supported format)
 * end_date: date (Optional, momentjs library supported format)
 *
 * @param data
 * @returns {Array<MockFunctionResult>|CLIEngine.LintResult[]|string|number|SpeechRecognitionResultList|*}
 * @constructor
 */
export const RESULT_MAPPER = (data) => {
    if (data.data.results) {
        return data.data.results;
    } else if (data.data.top) {
        return data.data.top;
    }
}


/**
 *
 * episode_id String
 * title String
 *
 * @param id The mal_id taken from the result mapper
 * @returns {Promise<void>}
 * @constructor
 */
export const GET_EPISODES_REQUEST = async (id) => {
    let res = await BaseAxios.request({
        url: GET_ANIME_EPISODE_LIST(id),
        method: "GET"
    });

    return res.data.episodes;
}

/**
 *
 * @param id The mal_id taken from the result mapper
 * @returns {Promise<*>}
 * @constructor
 */
export const GET_PICTURE_REQUEST = async (id) => {
    let res = await BaseAxios.request({
        url: GET_ANIME_PICTURES_LIST(id),
        method: "GET"
    });

    return res.data.pictures;
}







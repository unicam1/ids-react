export const ERROR_FORBIDDEN = 403;
export const ERROR_UNAUTH = 401;


export const SIGNIN = "/api/v1/users/signin";
export const GET_SEDE = "/api/v1/sede";
export const GET_UTENTE = "/api/v1/users";
export const GET_STORICO_DONAZIONI = "/api/v1/storicodonazione";
export const POST_SEDE = "/api/v1/sede";
export const REGISTER = "/api/v1/users";
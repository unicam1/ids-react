import React from "react";
import {GET_SEDE, REGISTER} from "../../constants/ApiHelpers/ApiConst";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import BaseAxios from "../../constants/ApiHelpers/BaseAxios";
import {Center, Container, Form, SimpleColumn} from "../../components/StyledDiv";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {Route} from "react-router-dom";


export default class RegisterScreen extends React.Component {


    state = {}
    REGISTER_STRUCTURE = [
        {field: "identifierCode", label: "identifierCode", default: "TEST"},
        {field: "name", label: "name", default: "TEST"},
        {field: "email", label: "email", default: "TEST"},
        {field: "cognome", label: "cognome", default: "TEST"},
        {field: "genere", label: "genere", default: "M"},
        {field: "cittaNascita", label: "cittaNascita", default: "TEST"},
        {field: "provinciaNascita", label: "provinciaNascita", default: "TEST"},
        {field: "statoNascita", label: "statoNascita", default: "TEST"},
        {field: "citta", label: "citta", default: "TEST"},
        {field: "provincia", label: "provincia", default: "TEST"},
        {field: "stato", label: "stato", default: "TEST"},
        {field: "numeroTelefono", label: "numeroTelefono", default: "TEST"},
        {field: "numeroTelefonoAlternativo", label: "numeroTelefonoAlternativo", default: "TEST"},
        {field: "indirizzo", label: "indirizzo", default: "TEST"},
        {field: "numeroCivico", label: "numeroCivico", default: "TEST"},
        {field: "cap", label: "cap", default: "TEST"},
        {field: "statoDomicilio", label: "statoDomicilio", default: "TEST"},
        {field: "provinciaDomicilio", label: "provinciaDomicilio", default: "TEST"},
        {field: "cittaDomicilio", label: "cittaDomicilio", default: "TEST"},
        {field: "indirizzoDomicilio", label: "indirizzoDomicilio", default: "TEST"},
        {field: "numeroCivicoDomicilio", label: "numeroCivicoDomicilio", default: "TEST"},
        {field: "capDomicilio", label: "capDomicilio", default: "TEST"},
        {field: "username", label: "username", default: "patient"},
        {field: "password", label: "password", default: "patient"},
        {
            field: "statoUtente", label: "Disponibilita donazione", default: 1, selectItem: [

                {key: 1, label: "Disponibile"},
                {key: 0, label: "Non disponibile"}
            ]
        }
    ]
    SEDE_STRUCTURE = [
        {field: "nomeSede", label: "nomeSede", default: "TEST"},
        {field: "indirizzoSede", label: "indirizzoSede", default: "TEST"},
        {field: "capSede", label: "capSede", default: "TEST"},
        {field: "provSede", label: "provSede", default: "TEST"},
        {field: "telSede", label: "telSede", default: 366262626},
        {field: "email", label: "email", default: "email@email.com"}
    ]

    handleSubmit = async (e) => {
        try {
            e.preventDefault();
            let data = {};
            let {selectedRowId, basicSurvey} = this.state;
            for (let i in this.REGISTER_STRUCTURE) {
                let temp = this.REGISTER_STRUCTURE[i];
                data[temp.field] = e.target[temp.field] ? e.target[temp.field].value : temp.default;
            }
            data.ruolo = "DN";
            // data.risposteQuestionario = JSON.stringify(basicSurvey);
            await BaseAxios.request({
                url: REGISTER + "/" + selectedRowId,
                method: "POST",
                data
            });
        } catch (error) {
            console.error("Error during register: ", error);
            this.setState({errorLogin: true});
        }
    };

    async componentDidMount() {
        await this.reloadData()
    }

    reloadData = async () => {
        try {

            let response = await BaseAxios.request({
                url: GET_SEDE,
                method: "GET"
            });
            this.setState({sedi: response.data})
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }
    }


    render() {
        let {selectedRowId} = this.state;
        return (
            <Container>
                <div>
                    {this.renderSedi()}
                    {selectedRowId && this.renderFormRegistrazione()}
                </div>
            </Container>
        )
    }

    renderFormRegistrazione() {
        let {selectedRowId, selectedRowName, basicSurvey} = this.state;
        return (<form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
            <Form width={"600px"}>
                <Grid container spacing={3}>
                    {this.REGISTER_STRUCTURE.map(a => {
                        return <Grid xs={6} item>
                            {!a.selectItem &&
                            <TextField key={a.field} id={a.field} label={a.label} defaultValue={a.default}
                                       variant={"outlined"} color={"primary"}/>}
                            {a.selectItem && (<div><label>{a.label}</label><Select
                                key={a.field} id={a.field}
                                onChange={(e) => {
                                    this.setState({role: e.target.value})
                                }}

                            >
                                {a.selectItem.map(v => {
                                    return <MenuItem value={v.key}> {v.label}</MenuItem>
                                })}
                            </Select></div>)}
                        </Grid>
                    })}
                    {/*{basicSurvey && basicSurvey.map(a => {*/}
                    {/*    return <Grid xs={6} item>*/}
                    {/*        {a.type == "text" && (*/}
                    {/*            <TextField key={a.label} id={a.label} label={a.label}*/}
                    {/*                       value={a.value || null}*/}
                    {/*                       variant={"outlined"} color={"primary"}*/}
                    {/*                       onChange={(event) => {*/}
                    {/*                           let temp = event.target.value*/}
                    {/*                           this.setState((p) => {*/}
                    {/*                               for (let e in p.basicSurvey) {*/}
                    {/*                                   if (p.basicSurvey[e].label === a.label) {*/}
                    {/*                                       p.basicSurvey[e].value = temp;*/}
                    {/*                                   }*/}
                    {/*                               }*/}

                    {/*                               return p*/}
                    {/*                           })*/}
                    {/*                       }}*/}

                    {/*            />)}*/}
                    {/*        {a.type == "select" && (<span>*/}
                    {/*                <p>{a.label}</p>*/}
                    {/*                <Select*/}
                    {/*                    value={a.value || null}*/}
                    {/*                    id={a.label}*/}
                    {/*                    key={a.label}*/}
                    {/*                    onChange={(event) => {*/}
                    {/*                        let temp = event.target.value*/}
                    {/*                        this.setState((p) => {*/}
                    {/*                            for (let e in p.basicSurvey) {*/}
                    {/*                                if (p.basicSurvey[e].label === a.label) {*/}
                    {/*                                    p.basicSurvey[e].value = temp;*/}
                    {/*                                }*/}
                    {/*                            }*/}

                    {/*                            return p*/}
                    {/*                        })*/}
                    {/*                    }}*/}

                    {/*                >*/}
                    {/*                {a.options.map(v => {*/}
                    {/*                    return <MenuItem value={v}>{v}</MenuItem>*/}
                    {/*                })}*/}
                    {/*            </Select></span>)}*/}
                    {/*    </Grid>*/}
                    {/*})}*/}
                    <Grid item>
                        <Button type={"submit"} color={"primary"} variant="outlined">Registrati nella
                            sede {selectedRowName}</Button>
                    </Grid>
                </Grid>
            </Form>
        </form>)
    }

    renderSedi() {
        let {sedi, selectedRowId} = this.state
        return (<SimpleColumn>
            {sedi && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.SEDE_STRUCTURE.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                            <TableCell style={{
                                head: {
                                    backgroundColor: "black",
                                    color: "white",
                                },
                                body: {
                                    fontSize: 14,
                                }
                            }}>Azioni</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {sedi.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idSede}>
                                {this.SEDE_STRUCTURE.map(a => {
                                    return <TableCell>{row[a.field]}</TableCell>
                                })}
                                <TableCell><Button color={"primary"} variant="outlined" onClick={() => {
                                    console.debug("Selected row : ", row.idSede)
                                    this.setState({
                                        selectedRowId: row.idSede,
                                        selectedRowName: row.nomeSede,
                                        basicSurvey: JSON.parse(row.survey)
                                    }, this.reloadData)
                                }}>Seleziona</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>);
    }


}

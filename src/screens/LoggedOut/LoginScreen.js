import React from "react";
import {GET_UTENTE, SIGNIN} from "../../constants/ApiHelpers/ApiConst";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import BaseAxios from "../../constants/ApiHelpers/BaseAxios";
import {AUTH_TOKEN_ID} from "../../constants/CommonConstants";
import {Center, Container, Form} from "../../components/StyledDiv";
import {withRouter} from "react-router-dom";
import logo from '../../components/img/./logo.png';


class LoginScreen extends React.Component {


    handleSubmit = async (e) => {
        console.debug("Check value")
        try {
            e.preventDefault();
            let username = e.target["username"].value;
            let password = e.target["password"].value;

            let firstSignin = await BaseAxios.request({
                url: SIGNIN,
                method: "GET",
                auth: {
                    username,
                    password
                }
            });
            console.debug("Check value", firstSignin.data)
            sessionStorage.setItem(AUTH_TOKEN_ID, firstSignin.data);
            let myInfo = await BaseAxios.request({
                url: GET_UTENTE,
                method: "GET"
            });
            this.props.history.push("/" + myInfo.data.ruolo + '/dashboard')
        } catch (error) {
            console.error("Error during login: ", error.response);
            this.setState({errorLogin: true});
        }
    };


    render() {
        return (
            
            <Container >
                <Center>
                    <form noValidate autoComplete="off" onSubmit={this.handleSubmit}  >
                        <Form>
                            <Grid container spacing={2} direction={"column"}>
                            <Grid item >
                        
                           <center> <img src={logo} alt="logo" alt="AnAvis" className="logo"/></center>
                          
                                </Grid>
                              
                                <Grid item>
                                    <TextField id="username" label="Inserisci username"/>
                                </Grid>
                                <Grid item>
                                    <TextField id="password" label="Password" type="password"/>
                                </Grid>
                               
                                <Grid item>
                                    <Button type={"submit"} color={"primary"} variant="contained">Login</Button>
                                    <p>Vuoi esssere un donatore? <br></br><center><a href="/patient">Registrati</a></center></p>
                                </Grid>
                            </Grid>
                        </Form>
                    </form>
                </Center>
            </Container>
        )
    }

}


export default withRouter(LoginScreen)
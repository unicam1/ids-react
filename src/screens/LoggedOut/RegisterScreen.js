import React from "react";
import {REGISTER} from "../../constants/ApiHelpers/ApiConst";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import BaseAxios from "../../constants/ApiHelpers/BaseAxios";
import {Center, Container, Form} from "../../components/StyledDiv";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";


export default class RegisterScreen extends React.Component {

    REGISTER_STRUCTURE = [
        {field: "identifierCode", label: "identifierCode", default: "TEST"},
        {field: "name", label: "name", default: "TEST"},
        {field: "email", label: "email", default: "TEST"},
        {field: "cognome", label: "cognome", default: "TEST"},
        {field: "genere", label: "genere", default: "M"},
        {field: "cittaNascita", label: "cittaNascita", default: "TEST"},
        {field: "provinciaNascita", label: "provinciaNascita", default: "TEST"},
        {field: "statoNascita", label: "statoNascita", default: "TEST"},
        {field: "citta", label: "citta", default: "TEST"},
        {field: "provincia", label: "provincia", default: "TEST"},
        {field: "stato", label: "stato", default: "TEST"},
        {field: "numeroTelefono", label: "numeroTelefono", default: "TEST"},
        {field: "numeroTelefonoAlternativo", label: "numeroTelefonoAlternativo", default: "TEST"},
        {field: "indirizzo", label: "indirizzo", default: "TEST"},
        {field: "numeroCivico", label: "numeroCivico", default: "TEST"},
        {field: "cap", label: "cap", default: "TEST"},
        {field: "statoDomicilio", label: "statoDomicilio", default: "TEST"},
        {field: "provinciaDomicilio", label: "provinciaDomicilio", default: "TEST"},
        {field: "cittaDomicilio", label: "cittaDomicilio", default: "TEST"},
        {field: "indirizzoDomicilio", label: "indirizzoDomicilio", default: "TEST"},
        {field: "numeroCivicoDomicilio", label: "numeroCivicoDomicilio", default: "TEST"},
        {field: "capDomicilio", label: "capDomicilio", default: "TEST"},
        {field: "username", label: "username", default: "TEST"},
        {field: "password", label: "password", default: "TEST"},
        {
            field: "ruolo", label: "ruolo", default: "EA", selectItem: [
                // {key: "DN", label: "Donatore"},
                // {key: "DT", label: "Dottore"},
                // {key: "SA", label: "Segretaria"},
                // {key: "AA", label: "Amministratore"},
                {key: "EA", label: "Ente Amministratore"}
            ]
        },
        {field: "statoUtente", label: "statoUtente", default: 1},
    ]
    handleSubmit = async (e) => {
        try {
            e.preventDefault();
            let data = {};
            for (let i in this.REGISTER_STRUCTURE) {
                let temp = this.REGISTER_STRUCTURE[i];
                data[temp.field] = e.target[temp.field] ? e.target[temp.field].value : temp.default;
                console.log("Check register: ", temp.field, temp.default, e.target[temp.field] ? e.target[temp.field].value : temp.default)
            }
            data.ruolo = this.state.role;
            console.log("Check register: ", data, e.target)
            await BaseAxios.request({
                url: REGISTER,
                method: "POST",
                data
            });
        } catch (error) {
            console.error("Error during register: ", error);
            this.setState({errorLogin: true});
        }
    };


    render() {
        return (
            <Container>
                <Center>
                    <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                        <Form width={"600px"}>
                            <Grid container spacing={3}>
                                {this.REGISTER_STRUCTURE.map(a => {
                                    return <Grid xs={6} item>
                                        {!a.selectItem &&
                                        <TextField key={a.field} id={a.field} label={a.label} defaultValue={a.default}
                                                   variant={"outlined"} color={"primary"}/>}
                                        {a.selectItem && <Select
                                            key={a.field} id={a.field}
                                            onChange={(e)=>{
                                                this.setState({role:e.target.value})
                                            }}

                                        >
                                            {a.selectItem.map(v => {
                                                return <MenuItem value={v.key}> {v.label}</MenuItem>
                                            })}
                                        </Select>}
                                    </Grid>
                                })}
                                <Grid item>
                                    <Button type={"submit"} color={"primary"} variant="outlined">REGISTRATI</Button>
                                </Grid>
                            </Grid>
                        </Form>
                    </form>
                </Center>
            </Container>
        )
    }

}

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import React from "react";
import {GET_SEDE, GET_UTENTE, POST_SEDE, REGISTER} from "../../../constants/ApiHelpers/ApiConst";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import BaseAxios from "../../../constants/ApiHelpers/BaseAxios";
import {Center, Container, Form, SimpleColumn} from "../../../components/StyledDiv";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import MapIcon from '@material-ui/icons/Map';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import logo from '../../../components/img/./logo.png';

export default class Dashboard extends React.Component {

    state = {
        error: false,
        renderItem: 0
    }

    REGISTER_STRUCTURE = [
        {field: "identifierCode", label: "identifierCode", default: "IDF"},
        {field: "name", label: "name", default: "Mario"},
        {field: "email", label: "email", default: "test@tes.com"},
        {field: "cognome", label: "cognome", default: "Rossi"},
        {field: "genere", label: "genere", default: "M"},
        {field: "cittaNascita", label: "cittaNascita", default: "Ancona"},
        {field: "provinciaNascita", label: "provinciaNascita", default: "AN"},
        {field: "statoNascita", label: "statoNascita", default: "Italia"},
        {field: "citta", label: "citta", default: "Cerignola"},
        {field: "provincia", label: "provincia", default: "FG"},
        {field: "stato", label: "stato", default: "Italia"},
        {field: "numeroTelefono", label: "numeroTelefono", default: "3665254125"},
        {field: "numeroTelefonoAlternativo", label: "numeroTelefonoAlternativo", default: "123165465"},
        {field: "indirizzo", label: "indirizzo", default: "Via delle vie"},
        {field: "numeroCivico", label: "numeroCivico", default: "21"},
        {field: "cap", label: "cap", default: "62254"},
        {field: "username", label: "username", default: "default"},
        {field: "password", label: "password", default: "default"},
        {field: "statoUtente", label: "statoUtente", default: 1},
    ]

    SEDE_STRUCTURE = [
        {field: "nomeSede", label: "nomeSede", default: "TEST"},
        {field: "indirizzoSede", label: "indirizzoSede", default: "TEST"},
        {field: "capSede", label: "capSede", default: "TEST"},
        {field: "provSede", label: "provSede", default: "TEST"},
        {field: "telSede", label: "telSede", default: 366262626},
        {field: "email", label: "email", default: "email@email.com"}
    ]

    componentDidMount() {
        this.reloadData();
    }

    reloadData = async () => {
        try {

            let response = await BaseAxios.request({
                url: GET_SEDE,
                method: "GET"
            });
            this.setState({data: response.data})
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }
        try {
            let {selectedRowId} = this.state
            if (selectedRowId) {
                let response = await BaseAxios.request({
                    url: GET_UTENTE + "/AA/" + selectedRowId,
                    method: "GET"
                });
                this.setState({dataUsers: response.data})
            }
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }
    };

    registerSubmit = async (e) => {
        try {
            e.preventDefault();
            let data = {};
            let {selectedRowId} = this.state;
            for (let i in this.REGISTER_STRUCTURE) {
                let temp = this.REGISTER_STRUCTURE[i];
                data[temp.field] = e.target[temp.field] ? e.target[temp.field].value : temp.default;
                console.log("Check register: ", temp.field, temp.default, e.target[temp.field] ? e.target[temp.field].value : temp.default)
            }
            data.ruolo = "AA";
            console.log("Check register: ", data, e.target)
            await BaseAxios.request({
                url: REGISTER + "/" + selectedRowId,
                method: "POST",
                data
            });
        } catch (error) {
            console.error("Error during register: ", error);
            this.setState({errorLogin: true});
        } finally {
            this.reloadData();
        }
    };

    handleSubmit = async (e) => {
        try {
            e.preventDefault();
            let data = {};
            for (let i in this.SEDE_STRUCTURE) {
                let temp = this.SEDE_STRUCTURE[i];
                data[temp.field] = e.target[temp.field] ? e.target[temp.field].value : temp.default;
                console.log("Check register: ", temp.field, temp.default, e.target[temp.field] ? e.target[temp.field].value : temp.default)
            }
            console.log("Check register: ", data, e.target)

            let response = await BaseAxios.request({
                url: POST_SEDE,
                method: "post",
                data
            });
        } catch (error) {
            console.error("Error", error.response);
        } finally {
            this.reloadData();
        }
    };

    render() {
        let {error, data, dataUsers, renderItem} = this.state;

        return (
            <Container logged={true}>


                <Drawer
                    variant="persistent"
                    anchor="left"
                    open={true}
                    style={{width: "280px"}}
                >

                    <Divider/>
                    <List></List>
                    <center><img src={logo} alt="logo" alt="AnAvis" className="logo"/></center>
                    <List></List>
                    <Divider/>
                    <List>
                        {['Sedi nel territorio'].map((text, index) => (
                            <ListItem button onClick={() => {
                                this.setState({renderItem: index, selectedRowId: null})
                            }} key={text}>
                                <ListItemIcon>{index % 2 === 0 ? <MapIcon/> : <MapIcon/>}</ListItemIcon>
                                <ListItemText primary={text}/>
                            </ListItem>
                        ))}
                    </List>
                    <Divider/>
                </Drawer>
                {renderItem === 0 && this.renderFirstMenuItem(error, data)}
            </Container>
        )
    }

    renderSecondMenuItem() {
        let {dataUsers} = this.state
        return <SimpleColumn>

            <ExpansionPanel style={{marginBottom: "5px"}}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <p>Aggiungi un amministratore</p>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <form noValidate autoComplete="on" onSubmit={this.registerSubmit}>
                        <Form width={"600px"}>
                            <Grid container spacing={3}>
                                {this.REGISTER_STRUCTURE.map(a => {
                                    return <Grid xs={6} item>
                                        {!a.selectItem &&
                                        <TextField key={a.field} id={a.field} label={a.label} defaultValue={a.default}
                                                   variant={"outlined"} color={"primary"}/>}
                                        {a.selectItem && <Select
                                            key={a.field} id={a.field}
                                            onChange={(e) => {
                                                this.setState({role: e.target.value})
                                            }}

                                        >
                                            {a.selectItem.map(v => {
                                                return <MenuItem value={v.key}> {v.label}</MenuItem>
                                            })}
                                        </Select>}
                                    </Grid>
                                })}
                                <Grid item>
                                    <Button type={"submit"} color={"primary"} variant="outlined">REGISTRATI</Button>
                                </Grid>
                            </Grid>
                        </Form>
                    </form>

                </ExpansionPanelDetails>
            </ExpansionPanel>
            {dataUsers && dataUsers.length > 0 && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.REGISTER_STRUCTURE.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {dataUsers.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idSede}>
                                {this.REGISTER_STRUCTURE.map(a => {
                                    return <TableCell>{row[a.field]}</TableCell>
                                })}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>;
    }

    renderFirstMenuItem(error, data) {
        console.debug("CHeck data in reder first element: ", data, !error && data, error)
        let {selectedRowId} = this.state
        if (selectedRowId) {
            return this.renderSecondMenuItem(error, data)
        }
        return (<SimpleColumn>
            <ExpansionPanel style={{marginBottom: "5px"}}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <p> Crea una nuova sede qui:</p>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                        <Form>
                            <Grid container spacing={3}>
                                {this.SEDE_STRUCTURE.map(a => {
                                    return <Grid xs={3} item>
                                        <TextField key={a.field} id={a.field} label={a.label}
                                                   defaultValue={a.default}
                                                   variant={"outlined"} color={"primary"}/>
                                    </Grid>
                                })}
                                <Grid item>
                                    <Button type={"submit"} color={"primary"} variant="outlined">Salva la
                                        nuova sede</Button>
                                </Grid>
                            </Grid>
                        </Form>
                    </form>

                </ExpansionPanelDetails>
            </ExpansionPanel>
            {!error && data && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.SEDE_STRUCTURE.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idSede}>
                                {this.SEDE_STRUCTURE.map(a => {
                                    return <TableCell>{row[a.field]}</TableCell>
                                })}
                                <TableCell><Button color={"primary"} variant="outlined" onClick={() => {
                                    console.debug("Selected row : ", row.idSede)
                                    this.setState({selectedRowId: row.idSede}, this.reloadData)
                                }}>Dettaglio</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>);
    }
}

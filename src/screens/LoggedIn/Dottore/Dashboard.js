import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import React from "react";
import {GET_SEDE, GET_STORICO_DONAZIONI, GET_UTENTE, POST_SEDE, REGISTER} from "../../../constants/ApiHelpers/ApiConst";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import BaseAxios from "../../../constants/ApiHelpers/BaseAxios";
import {Center, Container, Form, SimpleColumn} from "../../../components/StyledDiv";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import moment from "moment";
import {Route} from "react-router-dom";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Switch from '@material-ui/core/Switch';


function PaperComponent(props) {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}

export default class Dashboard extends React.Component {

    state = {
        error: false,
        renderItem: 0,
        data: {}
    }


    STORICO_STRUCTURE = [
        {field: "gruppoSanguinio", label: "Gruppo sanguinio"},
        {field: "patologie", label: "Patologie"},
        {field: "descrizioneAnomalie", label: "Descrizione anomalie"},
        {field: "lvGlobuli", label: "Livello Globuli"},
        {field: "lvPiastrine", label: "Livello Piastrine"},
        {field: "quantitaSangueDonato", label: "Quantità Sangue Donato"}
    ]
    STORICO_STRUCTURE_VISUAL = [
        {field: "gruppoSanguinio", label: "Gruppo sanguinio"},
        {field: "patologie", label: "Patologie"},
        {field: "descrizioneAnomalie", label: "Descrizione anomalie"},
        {field: "lvGlobuli", label: "Livello Globuli"},
        {field: "lvPiastrine", label: "Livello Piastrine"},
        {field: "quantitaSangueDonato", label: "Quantità Sangue Donato"},
        {
            field: "time",
            label: "Ora disponibilità",
            default: moment().format("YYYY-MM-DDTHH:mm"),
            type: "datetime-local"
        },
        {field: "duration", label: "Durata del prelievo", default: "10", type: "numeric"}
    ]

    async componentDidMount() {
        let myInfo = await BaseAxios.request({
            url: GET_UTENTE,
            method: "GET"
        });
        this.setState({sedeid: myInfo.data.sede.idSede, myInfo: myInfo.data, sede: myInfo.data.sede}, this.reloadData)
    }

    reloadData = async () => {
        let {sedeid, myInfo} = this.state

        //Carica le donazioni
        try {
            let response = await BaseAxios.request({
                url: GET_STORICO_DONAZIONI + "/" + sedeid,
                method: "GET"
            });
            this.setState({
                donations: response.data.filter(x => {
                    return x.utenteId
                })
            })
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }

    };


    handleDonation = async (e) => {
        try {
            let {idPrenotazione, data} = this.state;
            e.preventDefault();
            await BaseAxios.request({
                url: GET_STORICO_DONAZIONI + "/" + idPrenotazione,
                method: "PUT",
                data
            });
        } catch (error) {
            console.error("Error", error.response);
        } finally {
            this.setState({open: false, data: {}}, this.reloadData);
        }
    };

    handleClose = () => {
        this.setState({open: false, data: {}});
    };

    render() {
        let {renderItem} = this.state;
        return (
            <Container logged={true}>
                <Drawer
                    variant="persistent"
                    anchor="left"
                    open={true}
                    style={{width: "250px"}}
                >

                    <Divider/>
                    <List>
                        {["Prenotazioni", "Emergenza sangue"].map((text, index) => (
                            <ListItem button style={index === renderItem ? {backgroundColor: "grey"} : {}}
                                      onClick={() => {
                                          this.setState({renderItem: index})
                                      }} key={text}>
                                <ListItemIcon>{index % 2 === 0 ? <InboxIcon/> : <MailIcon/>}</ListItemIcon>
                                <ListItemText primary={text}/>
                            </ListItem>
                        ))}
                    </List>
                    <Divider/>
                </Drawer>
                {renderItem === 0 && this.renderStorico()}
                {renderItem === 1 && this.renderEmergenza()}
            </Container>
        )
    }

    renderEmergenza() {
        let checked = this.state.sede.emergenza
        return (<div>
            <label>Imposta la sede presso la quale sei registrato in emergenza sangue</label>
            <Switch
                checked={checked == "T" ? true : false}
                onChange={async (event) => {
                    let checked = event.target.checked ? "T" : "F";
                    let {sede, sedeid} = this.state;
                    try {
                        sede.emergenza = checked
                        await BaseAxios.request({
                            url: GET_SEDE + "/" + sedeid,
                            method: "PUT",
                            data: sede
                        });
                    } catch (error) {
                        console.error("Error", error.response);
                    } finally {
                        this.setState({sede}, this.reloadData);
                    }
                }}
                name="checkedA"
                inputProps={{'aria-label': 'secondary checkbox'}}
            />
        </div>)
    }


    renderStorico() {
        let {donations, open, data} = this.state

        return <SimpleColumn>
            {donations && donations.length > 0 && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.STORICO_STRUCTURE_VISUAL.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                            <TableCell style={{
                                head: {
                                    backgroundColor: "black",
                                    color: "white",
                                },
                                body: {
                                    fontSize: 14,
                                }
                            }}>Nome</TableCell>
                            <TableCell style={{
                                head: {
                                    backgroundColor: "black",
                                    color: "white",
                                },
                                body: {
                                    fontSize: 14,
                                }
                            }}>Cognome</TableCell>
                            <TableCell style={{
                                head: {
                                    backgroundColor: "black",
                                    color: "white",
                                },
                                body: {
                                    fontSize: 14,
                                }
                            }}>Azioni</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {donations.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idStorico}>
                                {this.STORICO_STRUCTURE_VISUAL.map(a => {
                                    return <TableCell>{a.field == "time" ? moment(row[a.field]).format("YYYY/MM/DD hh:mm") : row[a.field]}</TableCell>
                                })}

                                <TableCell>{row.utente && row.utente.name}</TableCell>
                                <TableCell>{row.utente && row.utente.cognome}</TableCell>
                                <TableCell><Button color={"primary"} variant="outlined" onClick={() => {
                                    this.setState({
                                        idPrenotazione: row.idStorico,
                                        data: row,
                                        open: true
                                    }, this.reloadData)
                                }}>Modifica</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
            <Dialog
                fullScreen={true}
                open={open}
                onClose={this.handleClose}
                PaperComponent={PaperComponent}
                aria-labelledby="draggable-dialog-title"
            >
                <DialogTitle style={{cursor: 'move'}} id="draggable-dialog-title">
                    Registra dati visita/donazione
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Compila il form per i dati relativi alla donazione appena effettuata
                    </DialogContentText>
                    <Form width={"600px"}>
                        <Grid container spacing={3}>
                            {this.STORICO_STRUCTURE.map(a => {
                                return <Grid xs={6} item>
                                    {!a.selectItem &&
                                    <TextField key={a.field} id={a.field} label={a.label} value={data[a.field]}
                                               variant={"outlined"} color={"primary"} onChange={(e) => {
                                        let temp = e.target.value;
                                        this.setState(p => {
                                            p.data[a.field] = temp;
                                            return p;
                                        })
                                    }}
                                    />}
                                    {a.selectItem && <Select
                                        key={a.field} id={a.field}
                                        onChange={(e) => {
                                            let temp = e.target.value;
                                            this.setState(p => {
                                                p.data[a.field] = temp;
                                                return p;
                                            })
                                        }}>
                                        {a.selectItem.map(v => {
                                            return <MenuItem value={v.key}> {v.label}</MenuItem>
                                        })}
                                    </Select>}
                                </Grid>
                            })}
                        </Grid>
                    </Form>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={this.handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleDonation} color="primary">
                        Salva
                    </Button>
                </DialogActions>
            </Dialog>
        </SimpleColumn>;
    }

}

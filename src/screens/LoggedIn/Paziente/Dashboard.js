import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import React from "react";
import {GET_SEDE, GET_STORICO_DONAZIONI, GET_UTENTE, POST_SEDE, REGISTER} from "../../../constants/ApiHelpers/ApiConst";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import BaseAxios from "../../../constants/ApiHelpers/BaseAxios";
import {Center, Container, Form, SimpleColumn} from "../../../components/StyledDiv";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import moment from "moment";
import {Route} from "react-router-dom";
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

export default class Dashboard extends React.Component {

    state = {
        error: false,
        renderItem: 0,
    }


    STORICO_STRUCTURE = [
        {
            field: "time",
            label: "Ora disponibilità",
            default: moment().format("YYYY-MM-DDTHH:mm"),
            type: "datetime-local"
        },
        {field: "duration", label: "Durata del prelievo", default: "10", type: "numeric"}
    ]
    STORICO_STRUCTURE_VISUAL = [
        {field: "gruppoSanguinio", label: "Gruppo sanguinio"},
        {field: "patologie", label: "Patologie"},
        {field: "descrizioneAnomalie", label: "Descrizione anomalie"},
        {field: "lvGlobuli", label: "Livello Globuli"},
        {field: "lvPiastrine", label: "Livello Piastrine"},
        {field: "quantitaSangueDonato", label: "Quantità Sangue Donato"},
        {
            field: "time",
            label: "Ora disponibilità",
            default: moment().format("YYYY-MM-DDTHH:mm"),
            type: "datetime-local"
        },
        {field: "duration", label: "Durata del prelievo", default: "10", type: "numeric"}
    ]
    SEDE_STRUCTURE = [
        {field: "nomeSede", label: "nomeSede", default: "TEST"},
        {field: "indirizzoSede", label: "indirizzoSede", default: "TEST"},
        {field: "capSede", label: "capSede", default: "TEST"},
        {field: "provSede", label: "provSede", default: "TEST"},
        {field: "telSede", label: "telSede", default: 366262626},
        {field: "email", label: "email", default: "email@email.com"}
    ]


    async componentDidMount() {
        let myInfo = await BaseAxios.request({
            url: GET_UTENTE,
            method: "GET"
        });
        let emergenzaSangue = myInfo.data.statoUtente == 1 && myInfo.data.sede.emergenza == "T";
        this.setState({
            sedeid: myInfo.data.sede.idSede,
            myInfo: myInfo.data,
            disponibilitàEmergenza: emergenzaSangue,
            openSnackbar: emergenzaSangue
        }, this.reloadData)
    }

    reloadData = async () => {
        let {sedeid, myInfo} = this.state

        //Carica le sedi
        try {

            let response = await BaseAxios.request({
                url: GET_SEDE,
                method: "GET"
            });
            this.setState({sedi: response.data})
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }
        //Carica le donazioni
        try {
            let response = await BaseAxios.request({
                url: GET_STORICO_DONAZIONI + "/" + sedeid,
                method: "GET"
            });
            this.setState({
                donations: response.data.filter(x => {
                    return !x.utenteId && moment(x.time).isAfter(moment())
                }), myDonations: response.data.filter(x => {

                    return x.utenteId == myInfo.id;
                })
            })
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }

        //Carico la sede per leggere il questionario
        try {
            let response = await BaseAxios.request({
                url: GET_SEDE + "/" + sedeid,
                method: "GET"
            });

            if (response.data && response.data.survey) {
                this.setState({sede: response.data, basicSurvey: JSON.parse(response.data.survey)})
            }
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }

    };


    handleDonation = async (e) => {
        try {
            let {basicSurvey, idPrenotazione, donations, myInfo} = this.state;
            e.preventDefault();
            let data = {}
            for (let i in donations) {
                if (donations[i].idStorico == idPrenotazione) {
                    data = {...donations[i]};
                    break;
                }
            }
            data.risposteQuestionario = JSON.stringify(basicSurvey);
            data.utenteId = myInfo.id
            await BaseAxios.request({
                url: GET_STORICO_DONAZIONI + "/" + idPrenotazione,
                method: "PUT",
                data
            });
        } catch (error) {
            console.error("Error", error.response);
        } finally {
            this.setState({idPrenotazione: null}, this.reloadData);
        }
    };


    render() {
        let {renderItem, disponibilitàEmergenza, openSnackbar} = this.state;
        return (
            <Container logged={true}>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={openSnackbar}
                    autoHideDuration={false}
                    message="Perfavore prenota una donazione c'è un emergenza sangue"
                    action={
                        <React.Fragment>
                            <IconButton size="small" aria-label="close" color="inherit" onClick={() => {
                                this.setState({openSnackbar: false})
                            }}>
                                <CloseIcon fontSize="small"/>
                            </IconButton>
                        </React.Fragment>
                    }
                />
                <Drawer
                    variant="persistent"
                    anchor="left"
                    open={true}
                    style={{width: "250px"}}
                >
                    <Divider/>
                    <List>
                        {["Donazioni", "Prenotazioni effettuate"].map((text, index) => (
                            <ListItem button style={index === renderItem ? {backgroundColor: "grey"} : {}}
                                      onClick={() => {
                                          this.setState({renderItem: index})
                                      }} key={text}>
                                <ListItemIcon>{index % 2 === 0 ? <InboxIcon/> : <MailIcon/>}</ListItemIcon>
                                <ListItemText primary={text}/>
                            </ListItem>
                        ))}
                    </List>
                    <Divider/>
                </Drawer>
                {renderItem === 0 && <div>{this.renderSedi()}<Divider/>{this.renderStorico()}</div>}
                {renderItem === 1 && this.renderStoricoPersonale()}

            </Container>
        )
    }


    renderFormRegistrazione() {
        let {basicSurvey} = this.state;
        return (<form noValidate autoComplete="off" onSubmit={this.handleDonation}>
            <Form width={"600px"}>
                <label>Compila il questionario</label>
                <Grid container spacing={3}>
                    {basicSurvey && basicSurvey.map(a => {
                        return <Grid xs={6} item>
                            {a.type == "text" && (
                                <TextField key={a.label} id={a.label} label={a.label}
                                           value={a.value || null}
                                           variant={"outlined"} color={"primary"}
                                           onChange={(event) => {
                                               let temp = event.target.value
                                               this.setState((p) => {
                                                   for (let e in p.basicSurvey) {
                                                       if (p.basicSurvey[e].label === a.label) {
                                                           p.basicSurvey[e].value = temp;
                                                       }
                                                   }

                                                   return p
                                               })
                                           }}

                                />)}
                            {a.type == "select" && (<span>
                                    <p>{a.label}</p>
                                    <Select
                                        value={a.value || null}
                                        id={a.label}
                                        key={a.label}
                                        onChange={(event) => {
                                            let temp = event.target.value
                                            this.setState((p) => {
                                                for (let e in p.basicSurvey) {
                                                    if (p.basicSurvey[e].label === a.label) {
                                                        p.basicSurvey[e].value = temp;
                                                    }
                                                }

                                                return p
                                            })
                                        }}

                                    >
                                    {a.options.map(v => {
                                        return <MenuItem value={v}>{v}</MenuItem>
                                    })}
                                </Select></span>)}
                        </Grid>
                    })}
                    <Grid item>
                        <Button color={"secondary"} variant="outlined" onClick={() => {
                            this.setState({idPrenotazione: null}, this.reloadData)
                        }}>Torna alla selezione date</Button>
                        <Button type={"submit"} color={"primary"} variant="outlined">Salva prenotazione</Button>
                    </Grid>
                </Grid>
            </Form>
        </form>)
    }


    renderStorico() {
        let {donations: data, idPrenotazione} = this.state
        if (idPrenotazione) {
            return this.renderFormRegistrazione();
        }

        return <SimpleColumn>
            {data && data.length > 0 && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.STORICO_STRUCTURE.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                            <TableCell style={{
                                head: {
                                    backgroundColor: "black",
                                    color: "white",
                                },
                                body: {
                                    fontSize: 14,
                                }
                            }}>Azioni</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idStorico}>
                                {this.STORICO_STRUCTURE.map(a => {
                                    return <TableCell>{a.field == "time" ? moment(row[a.field]).format("YYYY/MM/DD hh:mm") : row[a.field]}</TableCell>
                                })}
                                <TableCell><Button color={"primary"} variant="outlined" onClick={() => {
                                    console.debug("Selected row storico : ", row)
                                    this.setState({
                                        idPrenotazione: row.idStorico
                                    }, this.reloadData)
                                }}>Prenota</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>;
    }


    renderSedi() {
        let {sedi} = this.state
        return (<SimpleColumn>
            {sedi && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.SEDE_STRUCTURE.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                            <TableCell style={{
                                head: {
                                    backgroundColor: "black",
                                    color: "white",
                                },
                                body: {
                                    fontSize: 14,
                                }
                            }}>Azioni</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {sedi.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idSede}>
                                {this.SEDE_STRUCTURE.map(a => {
                                    return <TableCell>{row[a.field]}</TableCell>
                                })}
                                <TableCell><Button color={"primary"} variant="outlined" onClick={() => {
                                    console.debug("Selected row : ", row.idSede)
                                    this.setState({
                                        sedeid: row.idSede,
                                        selectedRowName: row.nomeSede,
                                        basicSurvey: JSON.parse(row.survey)
                                    }, this.reloadData)
                                }}>Seleziona</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>);
    }


    renderStoricoPersonale() {
        let {myDonations: data} = this.state
        return <SimpleColumn>
            {data && data.length > 0 && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.STORICO_STRUCTURE_VISUAL.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idStorico}>
                                {this.STORICO_STRUCTURE_VISUAL.map(a => {
                                    return <TableCell>{a.field == "time" ? moment(row[a.field]).format("YYYY/MM/DD hh:mm") : row[a.field]}</TableCell>
                                })}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>;
    }


}

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import React from "react";
import {GET_SEDE, GET_STORICO_DONAZIONI, GET_UTENTE, POST_SEDE, REGISTER} from "../../../constants/ApiHelpers/ApiConst";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import BaseAxios from "../../../constants/ApiHelpers/BaseAxios";
import {Center, Container, Form, SimpleColumn} from "../../../components/StyledDiv";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import moment from "moment";
import logo from '../../../components/img/./logo.png';

export default class Dashboard extends React.Component {

    state = {
        error: false,
        renderItem: 0
    }

    REGISTER_STRUCTURE = [
        {field: "identifierCode", label: "identifierCode", default: "IDF"},
        {field: "name", label: "name", default: "Mario"},
        {field: "email", label: "email", default: "test@tes.com"},
        {field: "cognome", label: "cognome", default: "Rossi"},
        {field: "genere", label: "genere", default: "M"},
        {field: "cittaNascita", label: "cittaNascita", default: "Ancona"},
        {field: "provinciaNascita", label: "provinciaNascita", default: "AN"},
        {field: "statoNascita", label: "statoNascita", default: "Italia"},
        {field: "citta", label: "citta", default: "Cerignola"},
        {field: "provincia", label: "provincia", default: "FG"},
        {field: "stato", label: "stato", default: "Italia"},
        {field: "numeroTelefono", label: "numeroTelefono", default: "3665254125"},
        {field: "numeroTelefonoAlternativo", label: "numeroTelefonoAlternativo", default: "123165465"},
        {field: "indirizzo", label: "indirizzo", default: "Via delle vie"},
        {field: "numeroCivico", label: "numeroCivico", default: "21"},
        {field: "cap", label: "cap", default: "62254"},
        {field: "username", label: "username", default: "default"},
        {field: "password", label: "password", default: "default"},
        {field: "statoUtente", label: "statoUtente", default: 1},
    ]

    STORICO_STRUCTURE = [
        {field: "time", label: "Ora disponibilità",default: moment().format("YYYY-MM-DDTHH:mm"), type:"datetime-local"},
        {field: "duration", label: "Durata del prelievo", default: "10", type:"numeric"}
    ]
    STORICO_STRUCTURE_VISUAL = [
        {field: "gruppoSanguinio", label: "Gruppo sanguinio"},
        {field: "patologie", label: "Patologie"},
        {field: "descrizioneAnomalie", label: "Descrizione anomalie"},
        {field: "lvGlobuli", label: "Livello Globuli"},
        {field: "lvPiastrine", label: "Livello Piastrine"},
        {field: "quantitaSangueDonato", label: "Quantità Sangue Donato"},
        {field: "time", label: "Ora disponibilità",default: moment().format("YYYY-MM-DDTHH:mm"), type:"datetime-local"},
        {field: "duration", label: "Durata del prelievo", default: "10", type:"numeric"}
    ]

    async componentDidMount() {
        let myInfo = await BaseAxios.request({
            url: GET_UTENTE,
            method: "GET"
        });
        console.debug("CHeck moment format string 2017-05-24T10:30    ", moment().format("YYYY-MM-SSThh:mm"), this.STORICO_STRUCTURE)
        this.setState({sedeid: myInfo.data.sede.idSede}, this.reloadData)
    }

    reloadData = async () => {
        let {sedeid} = this.state

        //Carica i dottori
        try {
            let response = await BaseAxios.request({
                url: GET_UTENTE + "/DT/" + sedeid,
                method: "GET"
            });
            this.setState({doctors: response.data})
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }
        //Carica le segretarie
        try {
            let response = await BaseAxios.request({
                url: GET_UTENTE + "/SA/" + sedeid,
                method: "GET"
            });
            this.setState({operators: response.data})
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }
        //Carica le donazioni
        try {
            let response = await BaseAxios.request({
                url: GET_STORICO_DONAZIONI + "/" + sedeid,
                method: "GET"
            });
            this.setState({donations: response.data})
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }
        //Carica le con utenti
        try {
            let response = await BaseAxios.request({
                url: GET_STORICO_DONAZIONI + "/detail/" + sedeid,
                method: "GET"
            });
            this.setState({donationsHistory: response.data})
        } catch (error) {
            console.error("Error ", error.response);
            this.setState({error: true});
        }
    };

    registerDoctor = async (e) => {
        try {
            e.preventDefault();
            let data = {};
            let {sedeid} = this.state;
            for (let i in this.REGISTER_STRUCTURE) {
                let temp = this.REGISTER_STRUCTURE[i];
                data[temp.field] = e.target[temp.field] ? e.target[temp.field].value : temp.default;
                console.log("Check register: ", temp.field, temp.default, e.target[temp.field] ? e.target[temp.field].value : temp.default)
            }
            data.ruolo = "DT";
            console.log("Check register: ", data, e.target)
            await BaseAxios.request({
                url: REGISTER + "/" + sedeid,
                method: "POST",
                data
            });
        } catch (error) {
            console.error("Error during register: ", error);
            this.setState({errorLogin: true});
        } finally {
            this.reloadData();
        }
    };

    registerOperator = async (e) => {
        try {
            e.preventDefault();
            let data = {};
            let {sedeid} = this.state;
            for (let i in this.REGISTER_STRUCTURE) {
                let temp = this.REGISTER_STRUCTURE[i];
                data[temp.field] = e.target[temp.field] ? e.target[temp.field].value : temp.default;
                console.log("Check register: ", temp.field, temp.default, e.target[temp.field] ? e.target[temp.field].value : temp.default)
            }
            data.ruolo = "SA";
            console.log("Check register: ", data, e.target)
            await BaseAxios.request({
                url: REGISTER + "/" + sedeid,
                method: "POST",
                data
            });
        } catch (error) {
            console.error("Error during register: ", error);
            this.setState({errorLogin: true});
        } finally {
            this.reloadData();
        }
    };

    registerSlot = async (e) => {
        try {
            e.preventDefault();
            let data = {};
            for (let i in this.STORICO_STRUCTURE) {
                let temp = this.STORICO_STRUCTURE[i];
                data[temp.field] = e.target[temp.field] ? e.target[temp.field].value : temp.default;
                console.log("Check register: ", temp.field, temp.default, e.target[temp.field] ? e.target[temp.field].value : temp.default)
            }
            console.log("Check register: ", data, e.target)
            data.sedeIdSede = this.state.sedeid;
            let response = await BaseAxios.request({
                url: GET_STORICO_DONAZIONI,
                method: "post",
                data
            });
        } catch (error) {
            console.error("Error", error.response);
        } finally {
            this.reloadData();
        }
    };

    render() {
        let {renderItem} = this.state;

        return (
            <Container logged={true}>
                <Drawer
                    variant="persistent"
                    anchor="left"
                    open={true}
                    style={{width: "280px"}}
                >

                    <Divider/>
                    <List></List>
                    <center> <img src={logo} alt="logo" alt="AnAvis" className="logo"/></center><List></List>
                    <Divider/>
                    <List>
                        {["Registro dottori", "Registro segretarie", "Registro turni donazioni", "Visualizza donazioni"].map((text, index) => (
                            <ListItem button style={index === renderItem ? {backgroundColor: "#57c295"} : {}}
                                      onClick={() => {
                                          this.setState({renderItem: index, selectedRowId: null})
                                      }} key={text}>
                                <ListItemIcon>{index % 2 === 0 ? <AssignmentIndIcon/> : <LocalHospitalIcon/>}</ListItemIcon>
                                <ListItemText primary={text}/>
                            </ListItem>
                        ))}
                    </List>
                    <Divider/>
                </Drawer>
                {renderItem === 0 && this.renderSecondMenuItem()}
                {renderItem === 1 && this.renderOperators()}
                {renderItem === 2 && this.renderStorico()}
                {renderItem === 3 && this.renderStoricoVisual()}
            </Container>
        )
    }

    renderSecondMenuItem() {
        let {doctors: data} = this.state
        return <SimpleColumn>

            <ExpansionPanel style={{marginBottom: "5px"}}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <p>Aggiungi un dottore</p>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <form noValidate autoComplete="on" onSubmit={this.registerDoctor}>
                        <Form width={"600px"}>
                            <Grid container spacing={3}>
                                {this.REGISTER_STRUCTURE.map(a => {
                                    return <Grid xs={6} item>
                                        {!a.selectItem &&
                                        <TextField key={a.field} id={a.field} label={a.label} defaultValue={a.default}
                                                   variant={"outlined"} color={"primary"}/>}
                                        {a.selectItem && <Select
                                            key={a.field} id={a.field}
                                            onChange={(e) => {
                                                this.setState({role: e.target.value})
                                            }}>
                                            {a.selectItem.map(v => {
                                                return <MenuItem value={v.key}> {v.label}</MenuItem>
                                            })}
                                        </Select>}
                                    </Grid>
                                })}
                                <Grid item>
                                    <Button type={"submit"} color={"primary"} variant="outlined">REGISTRA</Button>
                                </Grid>
                            </Grid>
                        </Form>
                    </form>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            {data && data.length > 0 && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.REGISTER_STRUCTURE.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idSede}>
                                {this.REGISTER_STRUCTURE.map(a => {
                                    return <TableCell>{row[a.field]}</TableCell>
                                })}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>;
    }



    renderOperators() {
        let {operators: data} = this.state
        return <SimpleColumn>

            <ExpansionPanel style={{marginBottom: "5px"}}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <p>Aggiungi una segretaria</p>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <form noValidate autoComplete="on" onSubmit={this.registerOperator}>
                        <Form width={"600px"}>
                            <Grid container spacing={3}>
                                {this.REGISTER_STRUCTURE.map(a => {
                                    return <Grid xs={6} item>
                                        {!a.selectItem &&
                                        <TextField key={a.field} id={a.field} label={a.label} defaultValue={a.default}
                                                   variant={"outlined"} color={"primary"}/>}
                                        {a.selectItem && <Select
                                            key={a.field} id={a.field}
                                            onChange={(e) => {
                                                this.setState({role: e.target.value})
                                            }}

                                        >
                                            {a.selectItem.map(v => {
                                                return <MenuItem value={v.key}> {v.label}</MenuItem>
                                            })}
                                        </Select>}
                                    </Grid>
                                })}
                                <Grid item>
                                    <Button type={"submit"} color={"secondary"} variant="outlined">REGISTRA</Button>
                                </Grid>
                            </Grid>
                        </Form>
                    </form>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            {data && data.length > 0 && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.REGISTER_STRUCTURE.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idSede}>
                                {this.REGISTER_STRUCTURE.map(a => {
                                    return <TableCell>{row[a.field]}</TableCell>
                                })}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>;
    }

    renderStorico() {
        let {donations: data} = this.state
        return <SimpleColumn>

            <ExpansionPanel style={{marginBottom: "5px"}}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <p>Aggiungi uno slot per le donazioni</p>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <form noValidate autoComplete="on" onSubmit={this.registerSlot}>
                        <Form width={"600px"}>
                            <Grid container spacing={3}>
                                {this.STORICO_STRUCTURE.map(a => {
                                    return <Grid xs={6} item>
                                        {!a.selectItem &&
                                        <TextField key={a.field} id={a.field} label={a.label} defaultValue={a.default} type={a.type}
                                                   variant={"outlined"} color={"primary"}/>}
                                        {a.selectItem && <Select
                                            key={a.field} id={a.field}
                                            onChange={(e) => {
                                                this.setState({role: e.target.value})
                                            }}

                                        >
                                            {a.selectItem.map(v => {
                                                return <MenuItem value={v.key}> {v.label}</MenuItem>
                                            })}
                                        </Select>}
                                    </Grid>
                                })}
                                <Grid item>
                                    <Button type={"submit"} color={"primary"} variant="outlined">REGISTRATI</Button>
                                </Grid>
                            </Grid>
                        </Form>
                    </form>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            {data && data.length > 0 && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.STORICO_STRUCTURE.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <TableRow style={{
                                root: { 
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idSede}>
                                {this.STORICO_STRUCTURE.map(a => {
                                    return <TableCell>{a.field == "time" ? moment(row[a.field]).format("YYYY/MM/DD hh:mm"): row[a.field]}</TableCell>
                                })}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>;
    }


    renderStoricoVisual() {
        let {donationsHistory: data} = this.state
        return <SimpleColumn>
            {data && data.length > 0 && <TableContainer component={Paper}>
                <Table stickyHeader aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {this.STORICO_STRUCTURE_VISUAL.map(a => {
                                return <TableCell style={{
                                    head: {
                                        backgroundColor: "black",
                                        color: "white",
                                    },
                                    body: {
                                        fontSize: 14,
                                    }
                                }}>{a.label}</TableCell>
                            })}
                            <TableCell style={{
                                head: {
                                    backgroundColor: "black",
                                    color: "white",
                                },
                                body: {
                                    fontSize: 14,
                                }
                            }}>Nome</TableCell>
                            <TableCell style={{
                                head: {
                                    backgroundColor: "black",
                                    color: "white",
                                },
                                body: {
                                    fontSize: 14,
                                }
                            }}>Cognome</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (
                            <TableRow style={{
                                root: {
                                    '&:nth-of-type(odd)': {
                                        backgroundColor: 'grey',
                                    }
                                }
                            }} key={row.idSede}>
                                {this.STORICO_STRUCTURE_VISUAL.map(a => {
                                    return <TableCell>{row[a.field]}</TableCell>
                                })}
                                <TableCell>{row.utente.nome}</TableCell>
                                <TableCell>{row.utente.cognome}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>}
        </SimpleColumn>;
    }

}

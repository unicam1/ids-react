import styled from "styled-components";

export const Container = styled.div`
display: flex;
height: 100vh;
background: #1ab394;
overflow: hidden;
padding: 20px;
margin-top:${props => props.logged ? "50px" : 0};
margin-left: ${props => props.drawerWidth ? props.drawerWidth : 0};
`;
export const Center = styled.div`
display: flex;
    min-height: 100vh;
    min-width: 100vw;
    justify-content: center;
    align-items: center;
flex-direction:column;
overflow: hidden;
`;

export const Form = styled.div`
display: flex;
flex-direction:column;
overflow: auto;
width: ${props => props.width ? props.width : "auto"};
border-radius: 20px;
background-color: white;
padding: 50px;
max-height: 50vh;
`;

export const SimpleColumn = styled.div`
display: flex;
flex-direction:column;
overflow: auto;
`;